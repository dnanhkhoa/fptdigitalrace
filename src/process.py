import cv2
import numpy as np
import collections


def display_frames(frames, show_label=True, delay=1):
    for i, frame in enumerate(frames):
        if show_label:
            cv2.putText(frame, 'Frame: %d' % i, (10, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0, 0, 255), 1, cv2.LINE_AA)
        cv2.imshow('frame', frame)
        if cv2.waitKey(delay) & 0xFF == ord('q'):
            break
    cv2.destroyAllWindows()


def load_frames(file_name):
    frames = []

    total_frames = 0
    width = 0
    height = 0
    fps = 0

    cap = cv2.VideoCapture(file_name)
    if cap.isOpened():
        # Read info
        total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
        width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        fps = int(cap.get(cv2.CAP_PROP_FPS))

        # Read frame
        flag, frame = cap.read()
        while flag:
            frames.append(frame)
            flag, frame = cap.read()
        # Free
        cap.release()
    return total_frames, width, height, fps, frames


def save_video(file_name, frames, fps, size):
    video = cv2.VideoWriter(file_name, -1, fps, size)
    if video.isOpened():
        for frame in frames:
            video.write(frame)
        video.release()


def process(frames, width, height, fps):
    abs_predicted = []
    predicted = []
    final_frames = []

    margin_top = 0
    margin_bottom = 0
    margin_left = 0
    margin_right = 0

    new_width = width
    new_height = height

    if len(frames) > 0:
        margin_top, margin_bottom, margin_left, margin_right, new_width, new_height = compute_margin(frames[0])
        new_height //= 4
        print('T: %d, B: %d, L: %d, R: %d, W: %d, H: %d' % (
            margin_top, margin_bottom, margin_left, margin_right, new_width, new_height))

    line_segment_detector = cv2.createLineSegmentDetector()

    abs_pos = lambda x, y: (margin_left + x, margin_top + height - new_height + y)

    for frame in frames:

        gray_scale = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        cropped_roi = gray_scale[margin_top + new_height * 3:margin_bottom + 1, margin_left:margin_right + 1]

        # Pre-processing
        filtered_frame = cropped_roi
        filtered_frame = cv2.blur(filtered_frame, (5, 5))
        filtered_frame = cv2.medianBlur(filtered_frame, 5)
        filtered_frame = cv2.GaussianBlur(filtered_frame, (5, 5), 0)

        scaled_frame = np.float32(filtered_frame) / 255
        # Increase contrast
        scaled_frame **= 3
        scaled_frame *= 2
        #scaled_frame *= 2
        #scaled_frame **= 2

        scaled_frame[scaled_frame > 1.0] = 1.0
        scaled_frame *= 255

        cropped_roi = scaled_frame.astype(np.uint8)

        # Here
        #_, threshold = cv2.threshold(cropped_roi, 100, 255, cv2.THRESH_BINARY)
        # threshold = cv2.adaptiveThreshold(cropped_roi, 100, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

        # Remove noise while keeping edge
        # blur = cv2.bilateralFilter(cropped_roi, 9, 200, 10)
        # pts = np.array([[10, 5], [20, 30], [70, 20], [50, 10]], np.int32)
        # pts = pts.reshape((-1, 1, 2))
        # color_frame = cv2.polylines(color_frame, [pts], True, (0, 255, 255))
        # color_frame = cv2.fillConvexPoly(color_frame, pts, (0, 255, 255))

        # (x, y) = (x + left_margin, y + top_margin + height - new_height)

        #_, threshold = cv2.threshold(cropped_roi, 80, 255, cv2.THRESH_BINARY)

        #x = compute_avg_coord(threshold)

        color_frame = cv2.cvtColor(cropped_roi, cv2.COLOR_GRAY2BGR)


        # if len(x) == 2:
        #     cv2.circle(color_frame, (int(x[1]), int(x[0])), 3, (0, 255, 0), 3)

        lines, width, precision, nfa = line_segment_detector.detect(cropped_roi)

        # lines = None
        if lines is None:
            if len(predicted) > 0:
                predicted.append(predicted[-1])
                abs_predicted.append(abs_predicted[-1])
            else:
                predicted.append(((new_width + 1) // 2 - 1, (new_height + 1) // 2 - 1))
                abs_predicted.append(abs_pos((new_width + 1) // 2 - 1, (new_height + 1) // 2 - 1))
        else:

            left_line_segments = []
            right_line_segments = []

            # Compute coords
            for line in lines:
                for x1, y1, x2, y2 in line:
                    if x1 < 0: x1 = 0
                    if y1 < 0: y1 = 0
                    if x2 < 0: x2 = 0
                    if y2 < 0: y2 = 0

                    if x1 > new_width - 1: x1 = new_width - 1
                    if y1 > new_height - 1: y1 = new_height - 1
                    if x2 > new_width - 1: x2 = new_width - 1
                    if y2 > new_height - 1: y2 = new_height - 1

                    # Swap
                    if y1 > y2: x1, x2, y1, y2 = x2, x1, y2, y1

                    angle = compute_angle((x1, y1), (x2, y2))
                    length = compute_euclidean_distance((x1, y1), (x2, y2))

                    if angle < 20 or angle > 160: continue
                    if length < 15: continue

                    if 85 <= angle <= 95 and (x1 <= 50 or x1 >= new_width - 50 or -120 <= x1 - new_width // 2 <= 120): continue

                    if x2 < new_width // 2:  # Left lane
                        if angle < 50 or angle > 150: continue
                        left_line_segments.append(([(x1, y1), (x2, y2)], [angle], length))
                    else:
                        if angle > 130: continue
                        right_line_segments.append(([(x1, y1), (x2, y2)], [angle], length))

            # for (p1, p2), angle, length in left_line_segments:
            #     cv2.line(color_frame, (p1[0], p1[1]), (p2[0], p2[1]), (0, 0, 255), 3)
            #
            # for (p1, p2), angle, length in right_line_segments:
            #     cv2.line(color_frame, (p1[0], p1[1]), (p2[0], p2[1]), (0, 0, 255), 3)

            for (p1, p2), angle, length in left_line_segments:
                cv2.line(frame, abs_pos(int(p1[0]), int(p1[1])), abs_pos(int(p2[0]), int(p2[1])), (0, 0, 255), 3)

            for (p1, p2), angle, length in right_line_segments:
                cv2.line(frame, abs_pos(int(p1[0]), int(p1[1])), abs_pos(int(p2[0]), int(p2[1])), (0, 0, 255), 3)

                # cv2.circle(color_frame, (new_width // 2, 0), 3, (0, 255, 0), 3)

        final_frames.append(frame)

    #display_frames(final_frames, delay=50)
    #save_video('demo2.avi', final_frames, fps, (height, width))
    # show_frame_demo(frames, abs_predicted)
    return predicted, final_frames, (new_width, new_height)


def compute_avg_coord(image):
    coords = []
    for i in range(image.shape[0]):
        for j in range(image.shape[1]):
            if image[i][j] == 255:
                coords.append((i, j))
    return np.mean(coords, axis=0) if len(coords) > 0 else None


def compute_euclidean_distance(p1, p2):
    return np.sqrt((p2[0] - p1[0]) ** 2 + (p2[1] - p1[1]) ** 2)


def compute_angle(p1, p2):
    return np.arctan2(p2[1] - p1[1], p2[0] - p1[0]) * 180 / np.pi


def compute_margin(frame):
    frame_shape = frame.shape
    if frame.size > 0 and len(frame_shape) == 3:
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    margin_top = 0
    margin_bottom = frame_shape[0] - 1
    margin_left = 0
    margin_right = frame_shape[1] - 1

    middle_pos = frame_shape[0] // 2  # frame_shape[0] is height
    center_pos = frame_shape[1] // 2  # frame_shape[1] is width

    # Left margin
    for i in range(center_pos):
        if max(frame[0][i], frame[middle_pos][i], frame[frame_shape[0] - 1][i]) > 0:
            margin_left = i
            break

    # Right margin
    for i in range(frame_shape[1] - 1, center_pos, -1):
        if max(frame[0][i], frame[middle_pos][i], frame[frame_shape[0] - 1][i]) > 0:
            margin_right = i
            break

    # Top margin
    for i in range(middle_pos):
        if max(frame[i][0], frame[i][center_pos], frame[i][frame_shape[1] - 1]) > 0:
            margin_top = i
            break

    # Bottom margin
    for i in range(frame_shape[0] - 1, middle_pos, -1):
        if max(frame[i][0], frame[i][center_pos], frame[i][frame_shape[1] - 1]) > 0:
            margin_bottom = i
            break

    width = margin_right - margin_left + 1
    height = margin_bottom - margin_top + 1

    return margin_top, margin_bottom, margin_left, margin_right, width, height


def compute_distance(predicted_coords, gold_coords):
    if len(predicted_coords) != len(gold_coords) or len(gold_coords) == 0:
        return .0
    delta = (gold_coords - predicted_coords) ** 2
    delta = np.sqrt(np.sum(delta, axis=1))
    return np.sum(delta) / len(delta)


def load_gold(file_name):
    coords = []
    with open(file_name, 'r') as f:
        for line in f:
            parts = line.rstrip('\r\n').split(' ')
            if len(parts) > 1:
                coords.append([int(parts[1]), int(parts[2])])
    return np.array(coords)


def show_frame_demo(frames, coords, fps, size, delay=100):
    final_frames = []
    for i in range(len(frames)):
        frame = cv2.circle(frames[i], tuple(coords[i]), 5, (255, 0, 255), -1)
        final_frames.append(frame)
    #display_frames(final_frames, True, delay)
    save_video('demo.avi', final_frames, fps, size)


def show_demo(video_file, coords_file, delay=100):
    total_frames, width, height, fps, original_frames = load_frames(video_file)
    coords = load_gold(coords_file)
    show_frame_demo(original_frames, coords, fps, (width, height))


def demo():
    show_demo('../testcases/01.avi', '../testcases/01.txt')


def main():
    testcases_folder = '../testcases/'
    testcases_file = '01'
    testcases_video_extension = '.avi'
    testcases_gold_extension = '.txt'

    gold_coords = load_gold(testcases_folder + testcases_file + testcases_gold_extension)
    total_frames, width, height, fps, original_frames = load_frames(
        testcases_folder + testcases_file + testcases_video_extension)

    predicted_coords, final_frames, size = process(original_frames, width, height, fps)
    distance = compute_distance(predicted_coords, gold_coords)
    # save_video('demo.avi', final_frames, fps, size)
    print('Cosine distance: %f' % distance)


if __name__ == '__main__':
    main()
    #demo()
